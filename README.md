# FVTT PF2e Critical Range Modifier
A module for the Foundry VTT PF2e system which enables a GM to change the critical success and failure breakpoints away from +10/-10

## Use
The breakpoints are independently accessible in module settings, and take effect on automatic reload.
