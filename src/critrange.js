let setting = key => {
    return game.settings.get("pf2e-critical-range-modifier", key);
};

const registerSettings = function () {

    let modulename = "pf2e-critical-range-modifier";

    const debouncedReload = foundry.utils.debounce(function () { window.location.reload(); }, 100);

    game.settings.register(modulename, "crit-range-success", {
        name: "Critical Success Breakpoint",
        hint: "By how much does a check need to meet or exceed the DC to critically succeed?",
        scope: "client",
        config: true,
        default: 10,
        type: Number,
        onChange: debouncedReload
    });
	
    game.settings.register(modulename, "crit-range-failure", {
        name: "Critical Failure Breakpoint",
        hint: "By how much does a check need to fall below a DC to critically fail?",
        scope: "client",
        config: true,
        default: 10,
        type: Number,
        onChange: debouncedReload
    });
};

Hooks.on(
    "init",
    () => {
		registerSettings();
		CONFIG.PF2E.criticalRange = {success: setting("crit-range-success"), failure: setting("crit-range-success")};		
    }
);
